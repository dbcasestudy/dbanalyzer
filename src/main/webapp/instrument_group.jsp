<%--
    Document   : index
    Created on : 06-Aug-2018
    Author     : B12
--%>

<%@page contentType="text/html" session="true" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper" class="b12.casestudy.dbanalyzer.core.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <title>Deutsche Bank Analyzer</title>
        <script src="//code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
        <script src="js/common.js" type="text/javascript"></script>
        <script src="js/help.js" type="text/javascript"></script>
        <script src="http://d3js.org/d3.v3.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="resources/dashboard/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
        <link rel="stylesheet" href="resources/dashboard/ready.css">
        <link rel="stylesheet" href="resources/dashboard/demo.css">
        <link rel="stylesheet" type="text/css" href="resources/help.css">
        <link rel="stylesheet" type="text/css" href="resources/login.css">
        <link rel="stylesheet" type="text/css" href="resources/table.css">
        <link rel="stylesheet" type="text/css" href="resources/requirements.css">
        <link rel="stylesheet" href="resources/dashboard/custom_dashboard.css">


    </head>

    <body>
        <%@include file="help.jsp" %>
        <div class="wrapper" style="display: <%=session.getAttribute("user") == null ? "none" : "block"%>">

            <div class="main-header">
                <div class="logo-header">
                    <img style="height: 30px;" src="http://www.ninefeettall.com/wp-content/uploads/2017/02/Asset-34-1024x186.png" alt="">
                </div>
                <nav class="navbar navbar-header navbar-expand-lg">
                    <div class="container-fluid">

                        <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                            <li class="nav-item dropdown hidden-caret">
                                <a class="nav-link dropdown-toggle"
                                   href="#" onclick="displayHelp()" id="helpBtn"
                                   role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false"><i class="nav-item dropdown hidden-caret fa fa-question-circle"></i>
                                </a>
                            </li>
                            <li class="nav-item dropdown hidden-caret">
                                <a class="nav-link dropdown-toggle"
                                   href="#" onclick="logout()" id="logoutBtn"
                                   role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">Log out <i class="fa fa-sign-out-alt" style=""></i>
                                </a>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
            <div class="sidebar">
                <div class="scrollbar-inner sidebar-wrapper">
                    <div class="user">
                        <div class="photo">
                            <i class="fas fa-user-circle"></i>
                        </div>
                        <div class="info">
                            <a class="" data-toggle="collapse"  aria-expanded="true">
                                <span>
                                    <%=session.getAttribute("user")%>
                                    <span class="user-level">User</span>
                                </span>
                            </a>

                        </div>
                    </div>
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="index.jsp">
                                <i class="fas fa-hand-holding-usd"></i>
                                <p>Deals</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="instrument_group.jsp">
                                <i class="fas fa-wrench"></i>
                                <p>Instruments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="counterparty_group.jsp">
                                <i class="fas fa-user-tie"></i>
                                <p>Counterparties</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="main-panel">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Instrument Table</h4>
                                        <p class="card-category">
                                            Table containing data about instruments</p>
                                    </div>
                                    <div class="card-body">
                                        <div class="mapcontainer">
                                            <div id="instrument_table">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





            <br>
        </div>
    </body>

    <script src="js/instruments.js" type="text/javascript"></script>
    <script>
                                       $(document).ready(function () {

        <%boolean userAttr = session.getAttribute("user") != null;%>
                                           var isAuthorised = "<%=userAttr%>";

                                           if (isAuthorised == "true")
                                           {
                                               displayInstrumentTable();
                                           }
                                       });
    </script>
</html>
