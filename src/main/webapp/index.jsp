<%--
    Document   : index
    Created on : 06-Aug-2018
    Author     : B12
--%>

<%@page contentType="text/html" session="true" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper" class="b12.casestudy.dbanalyzer.core.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <title>Deutsche Bank Analyzer</title>
        <script src="//code.jquery.com/jquery-1.10.2.js"
        type="text/javascript"></script>
        <script src="http://d3js.org/d3.v3.js"></script>
        <script src="js/common.js" type="text/javascript"></script>
        <script src="js/login.js" type="text/javascript"></script>
        <script src="js/help.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="resources/dashboard/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
        <link rel="stylesheet" href="resources/dashboard/ready.css">
        <link rel="stylesheet" href="resources/dashboard/demo.css">
        <link rel="stylesheet" type="text/css" href="resources/help.css">
        <link rel="stylesheet" type="text/css" href="resources/login.css">
        <link rel="stylesheet" type="text/css" href="resources/table.css">
        <link rel="stylesheet" type="text/css" href="resources/welcome.css">
        <link rel="stylesheet" type="text/css" href="resources/requirements.css">

        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" href="resources/dashboard/custom_dashboard.css">




    </head>

    <body>
        <%@include file="help.jsp" %>
        <%
            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();

            if (connectionStatus) {
        %>

        <!--Login form-->
        <form action="" method="post" id="loginForm" style="display: <%=session.getAttribute("user") == null ? "block" : "none"%>">


            <div class="container">
                <div class="imgcontainer">
                    <img src="http://www.stickpng.com/assets/images/5a1d2ccf4ac6b00ff574e27d.png" alt="Logo" class="logo">
                </div>
                <input type="text" placeholder="Enter Username" name="username" id="username">
                <input type="password" placeholder="Enter Password" name="password" id="password">
                <button type="button" id="loginBtn">Login <i class="fa fa-sign-in-alt"></i></button>
                <div id="helpBtn" onclick="displayHelp()" >Help <i class="fa fa-question-circle"></i></div>
            </div>
        </form>


        <!--Deal table page-->
        <div id="mainForm"  class="wrapper" style="display: <%=session.getAttribute("user") == null ? "none" : "block"%>">

            <div id="cover" class="cover-div"  style="display: none">

                <div class="logo_cover" style="display: none">

                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Deutsche_Bank_logo_without_wordmark.svg/1024px-Deutsche_Bank_logo_without_wordmark.svg.png" alt="Logo">

                </div>
                <br> <br> <br>

                <h1 id="welcomeUserText" style="display: none">
                    Welcome back,
                </h1>

            </div>

            <div class="main-header">
                <div class="logo-header">
                    <img style="height: 30px;" src="http://www.ninefeettall.com/wp-content/uploads/2017/02/Asset-34-1024x186.png" alt="">
                </div>
                <nav class="navbar navbar-header navbar-expand-lg">
                    <div class="container-fluid">

                        <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                            <li class="nav-item dropdown hidden-caret">
                                <a class="nav-link dropdown-toggle"
                                   href="#" onclick="displayHelp()" id="helpBtn"
                                   role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false"><i class="nav-item dropdown hidden-caret fa fa-question-circle"></i>
                                </a>
                            </li>
                            <li class="nav-item dropdown hidden-caret">
                                <a class="nav-link dropdown-toggle"
                                   href="#" onclick="logout()" id="logoutBtn"
                                   role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">Log out <i class="fa fa-sign-out-alt" style=""></i>
                                </a>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
            <div class="sidebar">
                <div class="scrollbar-inner sidebar-wrapper">
                    <div class="user">
                        <div class="photo">
                            <i class="fas fa-user-circle"></i>
                        </div>
                        <div class="info">
                            <a class="" data-toggle="collapse"  aria-expanded="true">
                                <span>
                                    <div id="loggedUser"><%=session.getAttribute("user")%></div>
                                    <span class="user-level">User</span>
                                </span>
                            </a>

                        </div>
                    </div>
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="index.jsp">
                                <i class="fas fa-hand-holding-usd"></i>
                                <p>Deals</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="instrument_group.jsp">
                                <i class="fas fa-wrench"></i>
                                <p>Instruments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="counterparty_group.jsp">
                                <i class="fas fa-user-tie"></i>
                                <p>Counterparties</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="main-panel">
                <div class="content">
                    <div class="container-fluid">
                        <h4 class="page-title">Available Instruments</h4>

                        <div class="multiple-items">
                        </div>

                        <div class="row">
                            <div class="col-md">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Trade deals</h4>
                                        <p class="card-category">
                                            Table displaying detailed information about transactions conducted in the given time period</p>
                                    </div>
                                    <div class="card-body">
                                        <div class="mapcontainer">
                                            <div id="table">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>





            <br>
        </div>
        <%
        } else {
        %>
        <strong>Db not connected</strong>
        <%
            }
        %>
    </body>

    <script src="js/deal.js" type="text/javascript"></script>
    <script>
                                       $(document).ready(function () {

//

        <%boolean userAttr = session.getAttribute("user") != null;%>
                                           var isAuthorised = "<%=userAttr%>";

                                           if (isAuthorised == "true")
                                           {
                                               displayTable();
                                               var instr_request = $.ajax({
                                                   method: 'GET',
                                                   url: rootURL + '/table/instrument',
                                                   dataType: "json", // data type of response
                                                   success: function (data) {
                                                       data.shift();
                                                       console.log(data);
                                                       var i = 0;
                                                       data.forEach(function (elem) {
                                                           $('.multiple-items').append('<a href="instrument.jsp?id=' + data[i].instrumentID + '" class="instr_box"><h4 class="name_inst">' + data[i].instrumentName +
                                                                   '<i class="fas fa-chart-bar"></i></h4><hr><p class="price_inst">Ending Price: &nbsp£' + data[i].endingPrice + '</p></a>');
                                                           i++;
                                                       });

                                                       $('.multiple-items').slick({
                                                           infinite: true,
                                                           variableWidth: true,
                                                           autoplay: true,
                                                           slidesToScroll: 1,
                                                           autoplaySpeed: 1000,
                                                           nextArrow: $('#next'),
                                                           prevArrow: $('#revious')
                                                       });
                                                   }
                                               });


                                           }

                                       });
    </script>
</html>
