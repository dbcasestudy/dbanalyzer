/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */







// This function is then used to update the DOM in the web page
function setMessageUsingDOM(user)
{
    var userMessageElement = document.getElementById("userIdMessage");
    var color;
    var target = $("#userIdMessage");

    if (target !== null)
    {
        target.html("<p>DEBUG JSON Representation from the server: " + JSON.stringify(user) + "</p>");
        userMessageElement.style.color = "red";
        var items = [];

        var existingElement = $("#" + user.userID);

        if (existingElement.length > 0)
            $("#" + user.userID).remove();

        items.push("<li id='" + user.userID + "'>" + user.userID + "->" + user.userPwd + "</li>");

        var userList = $("<ul/>", {
            "class": "dbgrads",
            html: items.join("")
        });
        userList.appendTo("body");

        target.html("<p>JSON Representation from the server: " + JSON.stringify(user) + "</p>");
        color = "green";

        userMessageElement.style.color = color;
    } else
        target.html("<div>ERROR IN PAGE</div>");
}


var column_names = ["Deal Time", "Deal Type", "Deal Amount", "Deal Quantity", "Deal Cost", "Instrument", "Counterparty"];
var clicks = {deal_time: 0, deal_type: 0, deal_amount: 0, deal_quantity: 0, deal_cost: 0, instrument_name: 0, counterparty_name: 0};

var curr_page = 1;
var max_page = Math.ceil(2000 / 15);

var rows, row_entries, row_entries_no_anchor, row_entries_with_anchor;

var query = "";
var offset = 0;
var limit = 15;
var sort_column;
var sort_order = "ASC";  //ASC & DESC
var filter_column;
var filter_columntwo;
var filter_content;
var filter_contenttwo;
var data = ["Deal Time", "Deal Type", "Deal Amount", "Deal Quantity", "Deal Cost", "Instrument", "Counterparty"];

query += "?limit=" + limit;

// draw the table
d3.select("#table").append("div")
        .attr("id", "container")

d3.select("#container").append("div")
        .attr("id", "FilterableTable");

d3.select("#FilterableTable").append("h1")
        .attr("id", "title")
        .text("Deal Table");

d3.select("#FilterableTable").append("div")
        .attr("class", "SearchBar");

var select = d3.select(".SearchBar").append('select')
        .attr("id", "dropdown")
        .on('change', onchange);

d3.select(".SearchBar")
        .append("input")
        .attr("class", "SearchBar")
        .attr("id", "search")
        .attr("type", "text")
        .attr("placeholder", "Search...");

//d3.select(".SearchBar")
//        .append("input")
//        .attr("type", "button")
//        .attr("id", "addtest")
//        .attr("onclick", "reset()");



d3.select(".SearchBar")
        .append("div")
        .attr("id", "addtest")
        .attr("onclick", "addFilter()");

$(document).ready(function () {

    document.getElementById("addtest").innerHTML = "<i class=\"fas fa-plus-circle\"></i>";

});


var selecttwo;

function addFilter() {
//    document.getElementById("tablebody").style.marginTop = "10px";

    selecttwo = d3.select(".SearchBar").append("select")
            .attr("id", "dropdown2")
            .on('change', onchangetwo);

    selecttwo.selectAll('option')
            .data(data).enter()
            .append('option')
            .text(function (d) {
                return d;
            });

    d3.select(".SearchBar")
            .append("input")
            .attr("class", "SearchBar")
            .attr("id", "search2")
            .attr("type", "text")
            .attr("placeholder", "Search...");
    redraw();

    d3.select("#addtest")
            .attr("onclick", "")
            .attr("style", "opacity: 0");
}

select.selectAll('option')
        .data(data).enter()
        .append('option')
        .text(function (d) {
            return d;
        });

function onchange() {
    switch (d3.select('#dropdown').property('value')) {
        case "Deal Time":
            filter_column = "deal_time";
            break;
        case "Deal Type":
            filter_column = "deal_type";
            break;
        case "Deal Amount":
            filter_column = "deal_amount";
            break;
        case "Deal Quantity":
            filter_column = "deal_quantity";
            break;
        case "Deal Cost":
            filter_column = "deal_cost";
            break;
        case "Instrument":
            filter_column = "deal_instrument_name";
            break;
        case "Counterparty":
            filter_column = "deal_counterparty_name";
            break;
    }
    console.log(filter_column);
}
;

function onchangetwo() {
    switch (d3.select('#dropdown2').property('value')) {
        case "Deal Time":
            filter_columntwo = "deal_time";
            break;
        case "Deal Type":
            filter_columntwo = "deal_type";
            break;
        case "Deal Amount":
            filter_columntwo = "deal_amount";
            break;
        case "Deal Quantity":
            filter_columntwo = "deal_quantity";
            break;
        case "Deal Cost":
            filter_columntwo = "deal_cost";
            break;
        case "Instrument":
            filter_columntwo = "deal_instrument_name";
            break;
        case "Counterparty":
            filter_columntwo = "deal_counterparty_name";
            break;
    }
    console.log(filter_columntwo);
}
;

d3.select("#FilterableTable").append("div")
        .attr("class", "Pagination");

d3.select(".Pagination").append("input")
        .attr("type", "button")
        .attr("id", "forward")
        .attr("value", "<< ");

d3.select(".Pagination").append("input")
        .attr("type", "button")
        .attr("id", "prev")
        .attr("value", "Prev");

d3.select(".Pagination").append("input")
        .attr("type", "button")
        .attr("id", "firstpage")
        .attr("value", "1");

d3.select(".Pagination").append("text")
        .attr("id", "firstdots")
        .text("...");

d3.select(".Pagination").append("input")
        .attr("type", "button")
        .attr("id", "currpage")
        .attr("value", curr_page);

d3.select(".Pagination").append("text")
        .attr("id", "seconddots")
        .text("...");

d3.select(".Pagination").append("input")
        .attr("type", "button")
        .attr("id", "lastpage")
        .attr("value", max_page);

d3.select(".Pagination").append("input")
        .attr("type", "button")
        .attr("id", "next")
        .attr("value", "Next");

d3.select(".Pagination").append("input")
        .attr("type", "button")
        .attr("id", "backward")
        .attr("value", " >>");


var table = d3.select("#FilterableTable").append("div").
        append("table").attr("id", "tablebody");
table.append("thead").append("tr");

var headers = table.select("tr").selectAll("th")
        .data(column_names)
        .enter()
        .append("th")
        .attr('class', 'header')
        .text(function (d) {
            return d;
        }).on("click", function (d) {
    offset = 0;
    headers.attr('class', 'header');

    switch (d) {
        case "Deal Time":
            sort_column = "deal_time";
            break;
        case "Deal Type":
            sort_column = "deal_type";
            break;
        case "Deal Amount":
            sort_column = "deal_amount";
            break;
        case "Deal Quantity":
            sort_column = "deal_quantity";
            break;
        case "Deal Cost":
            sort_column = "deal_cost";
            break;
        case "Instrument":
            sort_column = "deal_instrument_name";
            break;
        case "Counterparty":
            sort_column = "deal_counterparty_name";
            break;
    }

    if (sort_order == "ASC") {
        sort_order = "DESC";
        this.className = 'des';
        curr_page = 1;
    } else if (sort_order == "DESC") {
        sort_order = "ASC";
        this.className = 'aes';
        curr_page = 1;
    }

    console.log("first     " + filter_content);
    console.log("second     " + filter_contenttwo);

    query = "?limit=" + limit + "&offset=" + offset * limit + "&sort=" + sort_column + "&sort_order=" + sort_order;
//    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";
    if (filter_content != null && filter_contenttwo == "") {
        query += "&column=" + filter_column + "&filter=" + filter_content;
        console.log(query);
    } else if (filter_content != null && filter_contenttwo == null) {
        query += "&column=" + filter_column + "&filter=" + filter_content;
        console.log(query);
    } else if (filter_content != null && filter_contenttwo != "") {
        query += "&column=" + filter_column + "," + filter_columntwo + "&filter=" + filter_content + "," + filter_contenttwo;
        console.log(query);
    }



    console.log(query);
    redraw();

});

d3.select(".Pagination").select("#forward").on("click", function (d) {
    offset--;
    curr_page -= 5;
    if (curr_page <= 0) {
        curr_page = 1;
    }
    query = "?limit=" + limit + "&offset=" + offset * limit
    query += (sort_column != null) ? "&sort=" + sort_column + "&sort_order=" + sort_order : "";
//    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";
    if (filter_content != null && filter_contenttwo == null) {
        query += "&column=" + filter_column + "&filter=" + filter_content;
    } else if (filter_content != null && filter_contenttwo != null) {
        if (filter_contenttwo == "") {
            query += "&column=" + filter_column + "&filter=" + filter_content;
        } else {
            query += "&column=" + filter_column + "," + filter_columntwo + "&filter=" + filter_content + "," + filter_contenttwo;
        }
    }
    console.log(query);
    redraw();
})

d3.select(".Pagination").select("#backward").on("click", function (d) {
    offset++;
    curr_page += 5;
    if (curr_page >= max_page) {
        curr_page = max_page;
    }
    query = "?limit=" + limit + "&offset=" + offset * limit;
    query += (sort_column != null) ? "&sort=" + sort_column + "&sort_order=" + sort_order : "";
//    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";
    if (filter_content != null && filter_contenttwo == null) {
        query += "&column=" + filter_column + "&filter=" + filter_content;
    } else if (filter_content != null && filter_contenttwo != null) {
        if (filter_contenttwo == "") {
            query += "&column=" + filter_column + "&filter=" + filter_content;
        } else {
            query += "&column=" + filter_column + "," + filter_columntwo + "&filter=" + filter_content + "," + filter_contenttwo;
        }
    }
    console.log(query);
    redraw();
})


d3.select(".Pagination").select("#next").on("click", function (d) {
    offset++;
    curr_page++;
    query = "?limit=" + limit + "&offset=" + offset * limit
    query += (sort_column != null) ? "&sort=" + sort_column + "&sort_order=" + sort_order : "";
//    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";

    if (filter_content != null && filter_contenttwo == null) {
        query += "&column=" + filter_column + "&filter=" + filter_content;
    } else if (filter_content != null && filter_contenttwo != null) {
        if (filter_contenttwo == "") {
            query += "&column=" + filter_column + "&filter=" + filter_content;
        } else {
            query += "&column=" + filter_column + "," + filter_columntwo + "&filter=" + filter_content + "," + filter_contenttwo;
        }
    }
    console.log(query);
    redraw();
})

d3.select(".Pagination").select("#prev").on("click", function (d) {
    offset--;
    curr_page--;
    query = "?limit=" + limit + "&offset=" + offset * limit;
    query += (sort_column != null) ? "&sort=" + sort_column + "&sort_order=" + sort_order : "";
    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";
    console.log(query);
    redraw();
})
d3.select(".Pagination").select("#backward").on("click", function (d) {
    offset++;
    curr_page += 5;
    if (curr_page >= max_page) {
        curr_page = max_page;
    }
    query = "?limit=" + limit + "&offset=" + offset * limit;
    query += (sort_column != null) ? "&sort=" + sort_column + "&sort_order=" + sort_order : "";
    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";
//    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";
    if (filter_content != null && filter_contenttwo == null) {
        query += "&column=" + filter_column + "&filter=" + filter_content;
    } else if (filter_content != null && filter_contenttwo != null) {
        if (filter_contenttwo == "") {
            query += "&column=" + filter_column + "&filter=" + filter_content;
        } else {
            query += "&column=" + filter_column + "," + filter_columntwo + "&filter=" + filter_content + "," + filter_contenttwo;
        }
    }
    console.log(query);
    redraw();
})

d3.select(".Pagination").select("#firstpage").on("click", function (d) {
    offset = 0;
    curr_page = 1;
    query = "?limit=" + limit + "&offset=" + offset * limit;
    query += (sort_column != null) ? "&sort=" + sort_column + "&sort_order=" + sort_order : "";
//    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";
    if (filter_content != null && filter_contenttwo == null) {
        query += "&column=" + filter_column + "&filter=" + filter_content;
    } else if (filter_content != null && filter_contenttwo != null) {
        if (filter_contenttwo == "") {
            query += "&column=" + filter_column + "&filter=" + filter_content;
        } else {
            query += "&column=" + filter_column + "," + filter_columntwo + "&filter=" + filter_content + "," + filter_contenttwo;
        }
    }
    console.log(query);
    redraw();
})

d3.select(".Pagination").select("#lastpage").on("click", function (d) {
    offset = max_page - 1;
    curr_page = max_page;
    query = "?limit=" + limit + "&offset=" + offset * limit;
    query += (sort_column != null) ? "&sort=" + sort_column + "&sort_order=" + sort_order : "";
//    query += (filter_column != null) ? "&column=" + filter_column + "&filter=" + filter_content : "";
    if (filter_content != null && filter_contenttwo == null) {
        query += "&column=" + filter_column + "&filter=" + filter_content;
    } else if (filter_content != null && filter_contenttwo != null) {
        if (filter_contenttwo == "") {
            query += "&column=" + filter_column + "&filter=" + filter_content;
        } else {
            query += "&column=" + filter_column + "," + filter_columntwo + "&filter=" + filter_content + "," + filter_contenttwo;
        }
    }
    console.log(query);
    redraw();
})


function redraw() {
    console.log(query);
    var request = $.ajax({
        method: 'GET',
        url: rootURL + '/table/view_deal' + query,
        dataType: "json", // data type of response

        success: function (data) {
            console.log("redraw called");
            var size = data[0].size;
            data.shift();
            d3.select("#currpage").attr("value", curr_page);

            if (curr_page == 1) {       //on first page
                d3.select("#forward").attr("style", "opacity: 0");
                d3.select("#prev").attr("style", "opacity: 0");
                d3.select("#firstpage").attr("style", "opacity: 0");
                d3.select("#firstdots").attr("style", "opacity: 0");
                d3.select("#seconddots").attr("style", "opacity: 1");
                d3.select("#lastpage").attr("style", "opacity: 1");
                d3.select("#next").attr("style", "opacity: 1");
                d3.select("#backward").attr("style", "opacity: 1");
            } else if (curr_page == max_page) {     //on the last page
                d3.select("#forward").attr("style", "opacity: 1");
                d3.select("#prev").attr("style", "opacity: 1");
                d3.select("#firstpage").attr("style", "opacity: 1");
                d3.select("#firstdots").attr("style", "opacity: 1");
                d3.select("#seconddots").attr("style", "opacity: 0");
                d3.select("#lastpage").attr("style", "opacity: 0");
                d3.select("#next").attr("style", "opacity: 0");
                d3.select("#backward").attr("style", "opacity: 0");
            } else {        //on the middle page
                d3.select("#forward").attr("style", "opacity: 0");
                d3.select("#prev").attr("style", "opacity: 1");
                d3.select("#firstpage").attr("style", "opacity: 1");
                d3.select("#firstdots").attr("style", "opacity: 1");
                d3.select("#seconddots").attr("style", "opacity: 1");
                d3.select("#lastpage").attr("style", "opacity: 1");
                d3.select("#next").attr("style", "opacity: 1");
                d3.select("#backward").attr("style", "opacity: 1");
            }

            //pagination applying filter
            var page_number = Math.ceil(size / 15);
            max_page = page_number;
            if (page_number == 0) {     //no result
                d3.select("#forward").attr("style", "opacity: 0");
                d3.select("#prev").attr("style", "opacity: 0");
                d3.select("#firstpage").attr("style", "opacity: 0");
                d3.select("#firstdots").attr("style", "opacity: 0");
                d3.select("#currpage").attr("style", "opacity: 0");
                d3.select("#seconddots").attr("style", "opacity: 0");
                d3.select("#lastpage").attr("style", "opacity: 0");
                d3.select("#next").attr("style", "opacity: 0");
                d3.select("#backward").attr("style", "opacity: 0");
            } else if (page_number == 1) {      //just one page
                d3.select("#forward").attr("style", "opacity: 0");
                d3.select("#prev").attr("style", "opacity: 0");
                d3.select("#firstpage").attr("style", "opacity: 0");
                d3.select("#firstdots").attr("style", "opacity: 0");
                d3.select("#currpage").attr("style", "opacity: 1");
                d3.select("#seconddots").attr("style", "opacity: 0");
                d3.select("#lastpage").attr("style", "opacity: 0");
                d3.select("#next").attr("style", "opacity: 0");
                d3.select("#backward").attr("style", "opacity: 0");
            } else if (page_number < 6) {            //more than one page
                d3.select("#currpage").attr("style", "opacity: 1");
                if (curr_page == 1) {       //on the first page, show second part
                    d3.select("#forward").attr("style", "opacity: 0");
                    d3.select("#prev").attr("style", "opacity: 0");
                    d3.select("#firstpage").attr("style", "opacity: 0");
                    d3.select("#firstdots").attr("style", "opacity: 0");
                    d3.select("#backward").attr("style", "opacity: 0");
                } else {            //not on the first page, show the first part
                    d3.select("#forward").attr("style", "opacity: 0");
                    d3.select("#prev").attr("style", "opacity: 1");
                    d3.select("#firstpage").attr("style", "opacity: 1");
                    d3.select("#firstdots").attr("style", "opacity: 1");
                    d3.select("#backward").attr("style", "opacity: 0");
                }
            } else {
                d3.select("#currpage").attr("style", "opacity: 1");
                if (curr_page == 1) {       //on the first page, show second part
                    d3.select("#forward").attr("style", "opacity: 0");
                    d3.select("#prev").attr("style", "opacity: 0");
                    d3.select("#firstpage").attr("style", "opacity: 0");
                    d3.select("#firstdots").attr("style", "opacity: 0");
                } else {            //not on the first page, show the first part
                    d3.select("#forward").attr("style", "opacity: 1");
                    d3.select("#prev").attr("style", "opacity: 1");
                    d3.select("#firstpage").attr("style", "opacity: 1");
                    d3.select("#firstdots").attr("style", "opacity: 1");
                }

            }

            d3.select("#lastpage").attr("value", max_page);

            d3.select("tbody").html("");

            // data bind
            rows = table.select("tbody").selectAll("tr")
                    .data(data, function (d) {
                        return d.dealID;
                    });

            // enter the rows
            rows.enter()
                    .append("tr")

            // enter td's in each row
            row_entries = rows.selectAll("td")
                    .data(function (d) {
//                        console.log(d.size);
                        return [d.dealTime, d.dealType, "&pound;" + d.dealAmount, d.dealQuantity, "&pound;" + d.dealCost,
                            '<a href="instrument.jsp?id=' + d.dealInstrumentId + '">' + d.dealInstrumentName + '</a>',
                            '<a href="counterparty.jsp?id=' + d.counterpartyId + '">' + d.dealCounterpartyName + '</a>'];
                    })
                    .enter()
                    .append("td")
                    .html(function (d) {
                        return d;
                    });



            d3.select("#search")
                    .on("keyup", function () { // filter according to skey pressed
                        filter_content = document.getElementById('search').value;
//                        console.log(filter_content);
                        if (filter_content == "") {
                            query = "?limit=" + limit + "&offset=" + offset * limit;
                        }

                        offset = 0;
                        curr_page = 1;
                        query = "?limit=" + limit + "&offset=" + offset * limit + "&column=" + filter_column + "&filter=" + filter_content;


                        redraw();
                    });

            d3.select("#search2")
                    .on("keyup", function () { // filter according to key pressed
                        filter_contenttwo = document.getElementById('search2').value;

                        offset = 0;
                        curr_page = 1;
                        if (filter_contenttwo == "") {
                            query = "?limit=" + limit + "&offset=" + offset * limit + "&column=" + filter_column + "&filter=" + filter_content;
                        } else {
                            query = "?limit=" + limit + "&offset=" + offset * limit + "&column=" + filter_column + "," + filter_columntwo
                                    + "&filter=" + filter_content + "," + filter_contenttwo;
                        }
                        redraw();
                    });


        }
    });
}

function displayTable() {

    var request = $.ajax({
        method: 'GET',
        url: rootURL + '/table/view_deal' + query,
        dataType: "json", // data type of response

        success: function (data) {
            console.log("displaytable called");
            console.log("imin");
            var size = data[0].size;
            data.shift();

            d3.select("#forward").attr("style", "opacity: 0");
            d3.select("#prev").attr("style", "opacity: 0");
            d3.select("#firstpage").attr("style", "opacity: 0");
            d3.select("#firstdots").attr("style", "opacity: 0");

            // draw table body with rows
            table.append("tbody");

            // data bind
            rows = table.select("tbody").selectAll("tr")
                    .data(data, function (d) {
                        return d.dealID;
                    });

            // enter the rows
            rows.enter()
                    .append("tr");

            // enter td's in each row
            row_entries = rows.selectAll("td")
                    .data(function (d) {
//                        console.log(d.size);
                        return [d.dealTime, d.dealType, "&pound;" + d.dealAmount, d.dealQuantity, "&pound;" + d.dealCost,
                            '<a href="instrument.jsp?id=' + d.dealInstrumentId + '">' + d.dealInstrumentName + '</a>',
                            '<a href="counterparty.jsp?id=' + d.counterpartyId + '">' + d.dealCounterpartyName + '</a>'];
                    })
                    .enter()
                    .append("td")
                    .html(function (d) {
                        return d;
                    });


            d3.select("#search")
                    .on("keyup", function () { // filter according to skey pressed
                        console.log("test");
                        filter_content = document.getElementById('search').value;
                        console.log(filter_content);
                        offset = 0;
                        curr_page = 1;
                        query = "?limit=" + limit + "&offset=" + offset * limit + "&column=" + filter_column + "&filter=" + filter_content;
                        redraw();
                    });

            d3.select("#search2")
                    .on("keyup", function () { // filter according to key pressed
                        console.log("test");
                        filter_contenttwo = document.getElementById('search2').value;
                        console.log(filter_contenttwo);

                        offset = 0;
                        curr_page = 1;
                        query = "?limit=" + limit + "&offset=" + offset * limit + "&column=" + filter_column + "," + filter_columntwo
                                + "&filter=" + filter_content + "," + filter_contenttwo;
                        redraw();
                    });

        }
    });


}

d3.select(self.frameElement).style("height", "780px").style("width", "1150px");
