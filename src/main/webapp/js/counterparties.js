var column_names = ["Counterparty Name", "Counterparty Status", "Counterparty Date Registered", "Realised profit", "Effective profit"];
// draw the table
d3.select("#counterparty_table").append("div")
        .attr("id", "container")

d3.select("#container").append("div")
        .attr("id", "FilterableTable");

d3.select("#FilterableTable").append("h1")
        .attr("id", "title")
        .text("Counterparty Table")

var table = d3.select("#FilterableTable").append("table");
table.append("thead").append("tr");

var headers = table.select("tr").selectAll("th")
        .data(column_names)
        .enter()
        .append("th")
        .text(function (d) {
            return d;
        });

var rows, row_entries;


function displayCounterpartyTable() {

    var request = $.ajax({
        method: 'GET',
        url: rootURL + '/table/counterparty',
        dataType: "json", // data type of response

        success: function (data) {

            data.shift();

            // draw table body with rows
            table.append("tbody")

            // data bind
            rows = table.select("tbody").selectAll("tr")
                    .data(data, function (d) {
                        return d.counterpartyId;
                    });

            // enter the rows
            rows.enter()
                    .append("tr")


            // enter td's in each row
            row_entries = rows.selectAll("td")
                    .data(function (d) {
                        return ['<a href="counterparty.jsp?id=' + d.counterpartyId + '">' + d.counterpartyName + '</a>',
                            d.counterpartyStatus,
                            d.counterpartyDateRegistered.substring(0, 10),
                            "&pound;" + parseInt(d.realisedProfit).toLocaleString('en-GB'),
                            "&pound;" + parseInt(d.effectiveProfit).toLocaleString('en-GB')];
                    })
                    .enter()
                    .append("td")
                    .html(function (d) {
                        return d;
                    })


        }
    });

    request.fail(function (jqXHR, textStatus, errorThrown)
    {
        alert("Request " + textStatus + ", invalid login details: " + fds);
    });
}

d3.select(self.frameElement).style("height", "780px").style("width", "1150px");
