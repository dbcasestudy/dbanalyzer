/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {


    function validateUserId()
    {
        loginFromForm();
    }

    document.getElementById("username").addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;
        if (key === 13) {
            validateUserId()
        }
    });

    document.getElementById("password").addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;
        if (key === 13) {
            validateUserId()
        }
    });

    document.getElementById("loginBtn").addEventListener("click", validateUserId,
            false);

    $("#loginBtn").submit(function (event) {
        loginFromForm()
        event.preventDefault();
    });

    function loginFromForm() {
        formdata = $('form');
        fds = formdata.serialize();
        var request = $.ajax({
            method: 'POST',
            url: rootURL + '/login',
            data: fds,

            success: function (result) {

                $("#loggedUser").html(result);
                $("#loginForm").css('display', 'none');
                $("#welcomeUserText").append(result);
                $("#cover").css('display', 'flex');
                $('.logo_cover').fadeIn(600);
                $('#welcomeUserText').fadeIn(600);
                $("#mainForm").css('display', 'block');
                displayTable();
                
                var instr_request = $.ajax({
                    method: 'GET',
                    url: rootURL + '/table/instrument',
                    dataType: "json", // data type of response
                    success: function (data) {
                        data.shift();
                        console.log(data);
                        var i = 0;
                        data.forEach(function (elem) {
                             $('.multiple-items').append('<a href="instrument.jsp?id=' + data[i].instrumentID +  '" class="instr_box"><h4 class="name_inst">' + data[i].instrumentName +
                                     '<i class="fas fa-chart-bar"></i></h4><hr><p class="price_inst">Ending Price: &nbsp;&pound;' + data[i].endingPrice + '</p></a>');
                             i++;
                        });

                        $('.multiple-items').slick({
                            infinite: true,
                            variableWidth: true,
                            autoplay: true,
                            slidesToScroll: 1,
                            autoplaySpeed: 1000,
                            nextArrow: $('#next'),
                            prevArrow: $('#revious')
                        });
                        
                        var timeoutID = window.setTimeout(function(){
                            $(".cover-div").fadeOut();
                            
                       }, 2000);
                    }
                });
 
                
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown)
        {
            alert("Request " + textStatus + ", invalid login details: " + fds);
        });
    }


});
