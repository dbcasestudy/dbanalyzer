$(document).ready(function () {
    createInstrumentChart();

    var instr_request = $.ajax({
        method: 'GET',
        url: rootURL + '/table/view_instrument',
        dataType: "json", // data type of response
        success: function (data) {

            var stats = $.grep(data, function (n, i) {
                return n.instrumentID === getParameterByName('id');
            });

            stats.forEach(function (elem) {
                if (elem.dealType === 'B') {
                    $('#counter_name').append(elem.instrumentName);

                    $('#avg_buy').append(Number(elem.avgAmount).toFixed(2));
                    $('#max_buy').append(Number(elem.maxAmount).toFixed(2));
                    $('#min_buy').append(Number(elem.minAmount).toFixed(2));
                    $('#end_buy').append(Number(elem.endingPrice).toFixed(2));
                } else {
                    $('#avg_sell').append(Number(elem.avgAmount).toFixed(2));
                    $('#max_sell').append(Number(elem.maxAmount).toFixed(2));
                    $('#min_sell').append(Number(elem.minAmount).toFixed(2));
                    $('#end_sell').append(Number(elem.endingPrice).toFixed(2));
                }
            });
        }
    });
});





function createInstrumentChart() {
    var svg = d3.select("svg"),
            margin = {top: 20, right: 80, bottom: 30, left: 50},
            width = svg.attr("width") - margin.left - margin.right,
            height = svg.attr("height") - margin.top - margin.bottom,
            g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var x = d3.scaleTime().range([0, width]),
            y = d3.scaleLinear().range([height, 0]),
            z = d3.scaleOrdinal(d3.schemeCategory10);
    var line = d3.line()
            .curve(d3.curveBasis)
            .x(function (d) {
                return x(d.date);
            })
            .y(function (d) {
                return y(d.amount);
            });

    var instrument_id = getParameterByName('id');

    if (!instrument_id)
    {
        alert("No id param");
        return;
    }

    var request = $.ajax({
        method: 'GET',
        url: rootURL + '/table/deal',
        dataType: "json", // data type of response
        data: {
            column: "deal_instrument_id",
            filter: instrument_id
        },
        success: function (data) {
            data.shift();

            var buyArray = new Array();
            var sellArray = new Array();
            for (var dataIt in data) {
                if (data[dataIt].dealType === 'B') {
                    buyArray.push({date: new Date(data[dataIt].dealTime + "2"), amount: parseInt(data[dataIt].dealAmount)});
                } else {
                    sellArray.push({date: new Date(data[dataIt].dealTime + "2"), amount: parseInt(data[dataIt].dealAmount)});
                }
            }

            var chartData = [{id: 'Buy', values: buyArray}, {id: 'Sell', values: sellArray}]

            x.domain(d3.extent(data, function (d) {
                return new Date(d.dealTime + "2");
            }));
            y.domain([
                d3.min(chartData, function (c) {
                    return d3.min(c.values, function (d) {
                        return d.amount;
                    });
                }),
                d3.max(chartData, function (c) {
                    return d3.max(c.values, function (d) {
                        return d.amount;
                    });
                })
            ]);
            z.domain(chartData.map(function (c) {
                return c.id;
            }));
            g.append("g")
                    .attr("class", "axis axis--x")
                    .attr("transform", "translate(0," + height + ")")
                    .call(d3.axisBottom(x))
                    .append("text")
                    .attr("x", 750)
                    .attr("dx", "7em")
                    .attr("dy", "1.5em")
                    .attr("fill", "#000")
                    .text("Time");

            g.append("g")
                    .attr("class", "axis axis--y")
                    .call(d3.axisLeft(y))
                    .append("text")
                    .attr("transform", "rotate(-90)")
                    .attr("y", 6)
                    .attr("dy", "0.71em")
                    .attr("fill", "#000")
                    .text("Amount");
            var dealType = g.selectAll(".dealType")
                    .data(chartData)
                    .enter().append("g")
                    .attr("class", "dealType");

            chartData[0].values.forEach(function (item, index, object) {
                if (isNaN(item.date) || isNaN(item.amount)) {
                    object.splice(index, 1);
                }
            });

            chartData[1].values.forEach(function (item, index, object) {
                if (isNaN(item.date) || isNaN(item.amount)) {
                    object.splice(index, 1);
                }
            });

            dealType.append("path")
                    .attr("class", "line")
                    .attr("d", function (d) {
                        return line(d.values);
                    })
                    .style("stroke", function (d) {
                        return z(d.id);
                    });
            dealType.append("text")
                    .datum(function (d) {
                        return {id: d.id, value: d.values[d.values.length - 1]};
                    })
                    .attr("transform", function (d) {
                        return "translate(" + x(d.value.date) + "," + y(d.value.amount) + ")";
                    })
                    .attr("x", 3)
                    .attr("dy", "0.35em")
                    .style("font", "10px sans-serif")
                    .text(function (d) {
                        return d.id;
                    });

        }
    });
    request.fail(function (jqXHR, textStatus, errorThrown)
    {
        alert("Request " + textStatus);
    });
}

function type(d, _, columns) {
    d.date = parseTime(d.date);
    for (var i = 1, n = columns.length, c; i < n; ++i)
        d[c = columns[i]] = +d[c];
    return d;
}
