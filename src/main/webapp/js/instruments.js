var column_names = ["Instrument Name", "Ending price"];
// draw the table
d3.select("#instrument_table").append("div")
        .attr("id", "container")

d3.select("#container").append("div")
        .attr("id", "FilterableTable");

d3.select("#FilterableTable").append("h1")
        .attr("id", "title")
        .text("Instrument Table")

var table = d3.select("#FilterableTable").append("table");
table.append("thead").append("tr");

var headers = table.select("tr").selectAll("th")
        .data(column_names)
        .enter()
        .append("th")
        .text(function (d) {
            return d;
        });

var rows, row_entries;


function displayInstrumentTable() {

    var request = $.ajax({
        method: 'GET',
        url: rootURL + '/table/instrument',
        dataType: "json", // data type of response

        success: function (data) {
            data.shift();

            // draw table body with rows
            table.append("tbody")

            // data bind
            rows = table.select("tbody").selectAll("tr")
                    .data(data, function (d) {
                        return d.instrumentID;
                    });

            // enter the rows
            rows.enter()
                    .append("tr")


            // enter td's in each row
            row_entries = rows.selectAll("td")
                    .data(function (d) {
                        return ['<a href="instrument.jsp?id=' + d.instrumentID + '">' + d.instrumentName + '</a>', "&pound;" + d.endingPrice];
                    })
                    .enter()
                    .append("td")
                    .html(function (d) {
                        return d;
                    })
        }
    });

    request.fail(function (jqXHR, textStatus, errorThrown)
    {
        alert("Request " + textStatus + ", invalid login details: " + fds);
    });
}

d3.select(self.frameElement).style("height", "780px").style("width", "1150px");
