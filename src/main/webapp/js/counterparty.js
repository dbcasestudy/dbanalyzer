$(document).ready(function () {
    counterparty_id = getParameterByName('id');
    colors = ["#491D76", "#172E7B", "#004E8C", "#0C8786", "#006C3B", "#569834", "#CBBE00", "#C18210", "#C26919", "#BB302D", "#810D70", "#B81118"]


    var instr_request = $.ajax({
        method: 'GET',
        url: rootURL + '/table/view_counterparty',
        dataType: "json", // data type of response
        data: {
            column: "counterparty_id",
            filter: counterparty_id
        },
        success: function (data) {
            var filteredData = $.grep(data, function (n, i) {
                return n.counterpartyId === counterparty_id;
            })

            dataForNetTrades = [];
            dataForDealsQuantity = [];
            dataForEndingPosPositive = [];
            dataForEndingPosNegative = [];
            dataForRealisedProfitPositive = [];
            dataForRealisedProfitNegative = [];
            dataForEffectiveProfitPositive = [];
            dataForEffectiveProfitNegative = [];

            total = {
                "netTrades": 0,
                "dealsQuantity": 0,
                "endingPosPositive": 0,
                "endingPosNegative": 0,

                "realisedProfit": 0,
                "realisedLoss": 0,
                "effectiveProfit": 0,
                "effectiveLoss": 0
            };
            var i = 0;
            filteredData.forEach(function (element) {
                total.netTrades += parseInt(element.netTrades);
                dataForNetTrades.push({"label": element.instrumentName, "value": parseInt(element.netTrades), "color": colors[i]})
                total.dealsQuantity += parseInt(element.dealQty);
                dataForDealsQuantity.push({"label": element.instrumentName, "value": parseInt(element.dealQty), "color": colors[i]})
                var endPos = parseInt(element.endingPos);
                if (endPos > 0)
                {
                    total.endingPosPositive += endPos;
                    dataForEndingPosPositive.push({"label": element.instrumentName, "value": endPos, "color": colors[i]})
                } else
                {
                    total.endingPosNegative += endPos;
                    dataForEndingPosNegative.push({"label": element.instrumentName, "value": -endPos, "color": colors[i]})
                }

                var rp = parseInt(element.realisedProfit);
                if (rp > 0)
                {
                    total.realisedProfit += rp;
                    dataForRealisedProfitPositive.push({"label": element.instrumentName, "value": rp, "color": colors[i]})
                } else
                {
                    total.realisedLoss += rp;
                    dataForRealisedProfitNegative.push({"label": element.instrumentName, "value": -rp, "color": colors[i]})
                }
                var ep = parseInt(element.effectiveProfit);
                if (ep > 0)
                {
                    total.effectiveProfit += ep;
                    dataForEffectiveProfitPositive.push({"label": element.instrumentName, "value": ep, "color": colors[i]})
                } else
                {
                    total.effectiveLoss += ep;
                    dataForEffectiveProfitNegative.push({"label": element.instrumentName, "value": -ep, "color": colors[i]})
                }
                i++;
            })

            createCharts();
        }
    });

    var instr_request = $.ajax({
        method: 'GET',
        url: rootURL + '/table/counterparty',
        dataType: "json", // data type of response
        data: {
            column: "counterparty_id",
            filter: counterparty_id
        },
        success: function (data) {
            var filteredData = $.grep(data, function (n, i) {
                return n.counterpartyId === counterparty_id;
            })
            $('#counter_name').append(filteredData[0].counterpartyName);

            var totalRealised = parseInt(filteredData[0].realisedProfit)
            var totalEffective = parseInt(filteredData[0].effectiveProfit)

            $('#profitDescr').append(
                    (totalRealised > 0
                            ? "Total realised profit: "
                            : "Total realised loss: ")
                    + "&pound;"
                    + totalRealised.toLocaleString('en-GB')
                    + (totalEffective > 0
                            ? ", total effective profit: "
                            : ", total effective loss: ")
                    + "&pound;"
                    + totalEffective.toLocaleString('en-GB'))

            $('#status').append(filteredData[0].counterpartyStatus);
            $('#reg').append(filteredData[0].counterpartyDateRegistered.substring(0, 10));
        }
    });
});

function createCharts() {
    var netTradesPie = new d3pie("netTradesChart", {
        "header": {
            "title": {
                "text": "Net trades",
                "fontSize": 24,
                "font": "nunito"
            },
            "subtitle": {
                "text": "Quantity of units sold: " + total.netTrades.toLocaleString('en-GB'),
                "color": "#999999",
                "fontSize": 14,
                "font": "nunito"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "nunito",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 590,
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": dataForNetTrades
        },
        "labels": {
            "outer": {
                "pieDistance": 32
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8
            }
        },
    });

    var dealsQtyPie = new d3pie("dealsQtyChart", {
        "header": {
            "title": {
                "text": "Deals quantity",
                "fontSize": 24,
                "font": "nunito"
            },
            "subtitle": {
                "text": "Total: " + total.dealsQuantity.toLocaleString('en-GB'),
                "color": "#999999",
                "fontSize": 14,
                "font": "nunito"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "nunito",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 590,
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": dataForDealsQuantity
        },
        "labels": {
            "outer": {
                "pieDistance": 32
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8
            }
        },
    });

    var endingPosPositivePie = new d3pie("endingPosPositiveChart", {
        "header": {
            "title": {
                "text": "Positive ending position",
                "fontSize": 24,
                "font": "nunito"
            },
            "subtitle": {
                "text": "Total: " + total.endingPosPositive.toLocaleString('en-GB'),
                "color": "#999999",
                "fontSize": 14,
                "font": "nunito"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "nunito",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 590,
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": dataForEndingPosPositive
        },
        "labels": {
            "outer": {
                "pieDistance": 32
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8
            }
        },
    });

    var endingPosNegativePie = new d3pie("endingPosNegativeChart", {
        "header": {
            "title": {
                "text": "Negative ending position",
                "fontSize": 24,
                "font": "nunito"
            },
            "subtitle": {
                "text": "Total: " + total.endingPosNegative.toLocaleString('en-GB'),
                "color": "#999999",
                "fontSize": 14,
                "font": "nunito"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "nunito",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 590,
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": dataForEndingPosNegative
        },
        "labels": {
            "outer": {
                "pieDistance": 32
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8,
                "styles": {
                    "backgroundOpacity": 0.4,
                    "fontSize": 10,
                }
            }
        },
    });

    var realisedProfitPositivePie = new d3pie("realisedProfitPositiveChart", {
        "header": {
            "title": {
                "text": "Realised profit",
                "fontSize": 24,
                "font": "nunito"
            },
            "subtitle": {
                "text": "Total: " + total.realisedProfit.toLocaleString('en-GB'),
                "color": "#999999",
                "fontSize": 14,
                "font": "nunito"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "nunito",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 590,
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": dataForRealisedProfitPositive
        },
        "labels": {
            "outer": {
                "pieDistance": 32
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8,
                "styles": {
                    "backgroundOpacity": 0.4,
                    "fontSize": 10,
                }
            }
        },
    });

    var realisedProfitNegativePie = new d3pie("realisedProfitNegativeChart", {
        "header": {
            "title": {
                "text": "Realised loss",
                "fontSize": 24,
                "font": "nunito"
            },
            "subtitle": {
                "text": "Total: " + total.realisedLoss.toLocaleString('en-GB'),
                "color": "#999999",
                "fontSize": 14,
                "font": "nunito"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "nunito",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 590,
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": dataForRealisedProfitNegative
        },
        "labels": {
            "outer": {
                "pieDistance": 32
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8,
                "styles": {
                    "backgroundOpacity": 0.4,
                    "fontSize": 10,
                }
            }
        },
    });

    var effectiveProfitPositivePie = new d3pie("effectiveProfitPositiveChart", {
        "header": {
            "title": {
                "text": "Effective profit",
                "fontSize": 24,
                "font": "nunito"
            },
            "subtitle": {
                "text": "Total: " + total.effectiveProfit.toLocaleString('en-GB'),
                "color": "#999999",
                "fontSize": 14,
                "font": "nunito"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "nunito",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 590,
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": dataForEffectiveProfitPositive
        },
        "labels": {
            "outer": {
                "pieDistance": 32
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8,
                "styles": {
                    "backgroundOpacity": 0.4,
                    "fontSize": 10,
                }
            }
        },
    });
    

    var effectiveProfitNegativePie = new d3pie("effectiveProfitNegativeChart", {
        "header": {
            "title": {
                "text": "Effective loss",
                "fontSize": 24,
                "font": "nunito"
            },
            "subtitle": {
                "text": "Total: " + total.effectiveLoss.toLocaleString('en-GB'),
                "color": "#999999",
                "fontSize": 14,
                "font": "nunito"
            },
            "titleSubtitlePadding": 9
        },
        "footer": {
            "color": "#999999",
            "fontSize": 10,
            "font": "nunito",
            "location": "bottom-left"
        },
        "size": {
            "canvasWidth": 590,
            "pieOuterRadius": "90%"
        },
        "data": {
            "sortOrder": "value-desc",
            "content": dataForEffectiveProfitNegative
        },
        "labels": {
            "outer": {
                "pieDistance": 32
            },
            "inner": {
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 11
            },
            "percentage": {
                "color": "#ffffff",
                "decimalPlaces": 0
            },
            "value": {
                "color": "#adadad",
                "fontSize": 11
            },
            "lines": {
                "enabled": true
            },
            "truncation": {
                "enabled": true
            }
        },
        "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: {value}, {percentage}%"
        },
        "effects": {
            "pullOutSegmentOnClick": {
                "effect": "linear",
                "speed": 400,
                "size": 8,
                "styles": {
                    "backgroundOpacity": 0.4,
                    "fontSize": 10,
                }
            }
        },
    });
}