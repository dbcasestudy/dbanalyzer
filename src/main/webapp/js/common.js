var rootURL = "rws/services";

var path = window.location.pathname;
var page = path.split("/").pop();

$(document).ready(function () {
    switch (page) {
        case '':
        case 'index.jsp':
            $('.sidebar .nav .nav-item')[0].classList.add("active");
            break;
        case 'instrument_group.jsp':
            $('.sidebar .nav .nav-item')[1].classList.add("active");
            break;
        case 'counterparty_group.jsp':
            $('.sidebar .nav .nav-item')[2].classList.add("active");
            break;
    }
});


$(document).ready(function () {

    document.getElementById("logoutBtn").addEventListener("click", logout,
            false);

});


function logout() {

    var request = $.ajax({
        method: 'POST',
        url: rootURL + '/logout',

        success: function (result) {
            window.location.href = 'index.jsp';
        }
    });
}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}