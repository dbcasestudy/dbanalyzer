<!--Help pop up-->
<div id="myModal" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <h2 class="modal-h2">Instruction for using this app</h2>
            <span class="close">&times;</span>
        </div>
        <div class="modal-body">
            <p>The Investment Services App is for customers who are interested in investing in the financial market.</p>
            <p>Please login with your credentials. You will be able to browse through the market data, use filter to
                view data in desirable order, and query for average price, profit value, etc. on the side bar</p>
            <p>Contact: DB Technology training Group B1.2</p>
        </div>
    </div>
</div>