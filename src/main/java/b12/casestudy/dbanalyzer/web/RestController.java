/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.web;

import b12.casestudy.dbanalyzer.core.CounterpartyController;
import b12.casestudy.dbanalyzer.core.DealController;
import b12.casestudy.dbanalyzer.core.InstrumentController;
import b12.casestudy.dbanalyzer.core.UserController;
import b12.casestudy.dbanalyzer.core.ViewCounterpartyController;
import b12.casestudy.dbanalyzer.core.ViewDealController;
import b12.casestudy.dbanalyzer.core.ViewInstrumentController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author B12
 */
@Path("/services")
public class RestController {

    final UserController userController = new UserController();

    final DealController dealController = new DealController();

    final InstrumentController instrumentController = new InstrumentController();

    final ViewInstrumentController viewInstrumentController = new ViewInstrumentController();

    final CounterpartyController counterpartyController = new CounterpartyController();

    final ViewDealController viewDealController = new ViewDealController();

    final ViewCounterpartyController viewCounterpartyController = new ViewCounterpartyController();

    @POST
    @Path("/login")
    @Produces({MediaType.TEXT_PLAIN})
    public Response postLoginForm(@Context HttpServletRequest req, @FormParam("username") String usr,
            @FormParam("password") String pwd) {
        String result = userController.verifyLoginDetails(usr, pwd);

        if (result != null) {
            HttpSession session = req.getSession(true);
            session.setAttribute("user", result);

            return Response.ok(result, MediaType.TEXT_PLAIN).build();
        } else {
            return Response.status(400).entity("User could not be found").build();
        }
    }

    @POST
    @Path("/logout")
    public Response postLoginForm(@Context HttpServletRequest req) {
        HttpSession session = req.getSession(true);
        session.setAttribute("user", null);
        return Response.ok().build();
    }

    @GET
    @Path("/table/deal")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTableDeal(@Context HttpServletRequest req,
            @QueryParam("column") String column,
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @QueryParam("sort_order") String sortOrder,
            @QueryParam("limit") String limit,
            @QueryParam("offset") String offset) {

        HttpSession session = req.getSession(true);
        if (session.getAttribute("user") == null) {
            return Response.status(401).encoding("User is not authorised").build();
        }

        String result = dealController.getTable(column, filter, sort, sortOrder,
                limit, offset);
        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/table/instrument")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTableInstrument(@Context HttpServletRequest req,
            @QueryParam("column") String column,
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @QueryParam("sort_order") String sortOrder,
            @QueryParam("limit") String limit,
            @QueryParam("offset") String offset) {

        HttpSession session = req.getSession(true);
        if (session.getAttribute("user") == null) {
            return Response.status(401).encoding("User is not authorised").build();
        }

        String result = instrumentController.getTable(column, filter, sort,
                sortOrder, limit, offset);
        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/table/counterparty")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTableCounterparty(@Context HttpServletRequest req,
            @QueryParam("column") String column,
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @QueryParam("sort_order") String sortOrder,
            @QueryParam("limit") String limit,
            @QueryParam("offset") String offset) {

        HttpSession session = req.getSession(true);
        if (session.getAttribute("user") == null) {
            return Response.status(401).encoding("User is not authorised").build();
        }

        String result = counterpartyController.getTable(column, filter, sort,
                sortOrder, limit, offset);
        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/table/view_instrument")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getViewInstrument(@Context HttpServletRequest req,
            @QueryParam("column") String column,
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @QueryParam("sort_order") String sortOrder,
            @QueryParam("limit") String limit,
            @QueryParam("offset") String offset) {

        HttpSession session = req.getSession(true);
        if (session.getAttribute("user") == null) {
            return Response.status(401).encoding("User is not authorised").build();
        }

        String result = viewInstrumentController.getTable(column, filter, sort,
                sortOrder, limit, offset);
        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/table/view_deal")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getViewDeal(@Context HttpServletRequest req,
            @QueryParam("column") String column,
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @QueryParam("sort_order") String sortOrder,
            @QueryParam("limit") String limit,
            @QueryParam("offset") String offset) {

        HttpSession session = req.getSession(true);
        if (session.getAttribute("user") == null) {
            return Response.status(401).encoding("User is not authorised").build();
        }

        String result = viewDealController.getTable(column, filter, sort,
                sortOrder, limit, offset);
        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/table/view_counterparty")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getViewCounterparty(@Context HttpServletRequest req,
            @QueryParam("column") String column,
            @QueryParam("filter") String filter,
            @QueryParam("sort") String sort,
            @QueryParam("sort_order") String sortOrder,
            @QueryParam("limit") String limit,
            @QueryParam("offset") String offset) {

        HttpSession session = req.getSession(true);
        if (session.getAttribute("user") == null) {
            return Response.status(401).encoding("User is not authorised").build();
        }

        String result = viewCounterpartyController.getTable(column, filter, sort,
                sortOrder, limit, offset);
        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }
}
