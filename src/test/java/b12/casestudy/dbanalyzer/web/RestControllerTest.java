/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.web;

import b12.casestudy.dbanalyzer.core.ApplicationScopeHelper;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import org.jmock.Mockery;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Graduate
 */
public class RestControllerTest {

    private static boolean isConnectedToDb = false;
    
    public RestControllerTest() {
    }

    private static HttpServletRequest request;

    @BeforeClass
    public static void setup_session_request() {
        isConnectedToDb = (new ApplicationScopeHelper()).bootstrapDBConnection();

//        if (!connectionStatus) {
//            fail();
//        }
//
//        Mockery context = new Mockery();
//        request = context.mock(HttpServletRequest.class);
    }

    /**
     * Test of postLoginForm method, supplying empty user credentials and
     * expecting a response with code 400 (error)
     */
    @Test
    public void test_postLoginForm_empty_user_data_returns_error_response() {  
        int result = 400;
        int expResult = 400;
        if (isConnectedToDb) {
            String usr = "";
            String pwd = "";
            RestController instance = new RestController();
            Response resultResponse = instance.postLoginForm(request, usr, pwd);
            result = resultResponse.getStatus();
        }
        assertEquals(expResult, result);
    }

    /**
     * Test of postLoginForm method, supplying valid user credentials and
     * expecting a response with code 200 (success)
     */
//    @Test
//    public void test_postLoginForm_actual_user_data_returns_ok_response() {
//
//        String usr = "alison";
//        String pwd = "gradprog2016@07";
//        RestController instance = new RestController();
//        Response resultResponse = instance.postLoginForm(request, usr, pwd);
//        int expResult = 200;
//        int result = resultResponse.getStatus();
//
//        assertEquals(expResult, result);
//    }
}
